package org.droidtr.sysapi;

import android.widget.CheckedTextView;
import java.lang.reflect.Field;

public class CheckedTextViewUtils {
	
	private CheckedTextViewUtils(){}
	
	public static int getCheckMarkGravity(CheckedTextView ctv){
		try {
			return getCheckMarkGravityField(ctv).get(ctv);
		} catch(Throwable t){}
		return -1;
	}
	
	public static void setCheckMarkGravity(CheckedTextView ctv, int gravity){
		try {
			getCheckMarkGravityField(ctv).set(ctv, gravity);
			ctv.requestLayout();
			ctv.invalidate();
		} catch(Throwable t){}
	}
	
	private static Field getCheckMarkGravityField(CheckedTextView ctv) throws NoSuchFieldException {
		Field fld = CheckedTextView.class.getDeclaredField("mCheckMarkGravity");
		fld.setAccessible(true);
		return fld;
	}
	
}
