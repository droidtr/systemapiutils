package org.droidtr.sysapi;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.os.AsyncTask;
import android.os.SystemClock;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.net.InetAddress;

import static android.os.Build.VERSION.SDK_INT;

/**
 * SntpClientUtils class
 *
 * Access SntpClient class with reflection
 * and get time asynchronously
 *
 * @author frknkrc44
 */
public class SntpClientUtils {
	
	// SntpClient object
	private static Object sntpClient;
	
	// SntpClient class
	private static Class<?> CLASS;
	
	// Context required for SDK >= PIE
	private static Context mCtx;
	
	// ntp port and timeout values
	private static final int NTP_PORT = 123, TIMEOUT = 100000;
	
	// AsyncTask postExecute listener
	private static ClockTaskExecutor exec;
	
	public SntpClientUtils(){
		init();
	}
	
	public SntpClientUtils(Context ctx){
		mCtx = ctx;
		init();
	}
	
	/**
	 * Init method for SntpClient
	 * 
	 * Implements SntpClient class and 
	 * implements a SntpClient object as Object
	 * also controls Context if SDK >= PIE
	 */
	private void init(){
		if(SDK_INT >= 28 && mCtx == null){
			throw new RuntimeException("You must implement this object with Context");
		}

		try {
			CLASS = Class.forName("android.net.SntpClient");
			Constructor cs = CLASS.getConstructor();
			cs.setAccessible(true);
			sntpClient = cs.newInstance();
		} catch(Throwable t){
			throw new RuntimeException(t);
		}
	}
	
	/**
	 * Requests time from default time server
	 * with AsyncTask and listener
	 * Default time server is hardcoded to "pool.ntp.org"
	 */
	public void requestTime(ClockTaskExecutor clkExec){
		requestTime(clkExec,"pool.ntp.org");
	}
	
	/**
	 * Requests time from selected time server
	 * with AsyncTask and listener
	 */
	public void requestTime(ClockTaskExecutor clkExec, String host){
		exec = clkExec;
		exec.start(host);
	}
	
	/**
	 * Get NTP time (millis)
	 */
	public long getNtpTime(){
		return getValue("getNtpTime");
	}
	
	/**
	 * Get NTP time reference (millis)
	 */
	public long getNtpTimeReference(){
		return getValue("getNtpTimeReference");
	}
	
	/**
	 * Get round time of NTP time (millis)
	 */
	public long getRoundTripTime(){
		return getValue("getRoundTripTime");
	}
	
	/**
	 * Get current time (millis)
	 */
	public long getNow(){
		return getNtpTime() + SystemClock.elapsedRealtime() - getNtpTimeReference();
	}
	
	/**
	 * Access hidden time methods with reflection
	 */
	private long getValue(String name){
		try {
			Method method = CLASS.getDeclaredMethod(name);
			method.setAccessible(true);
			return (long) method.invoke(sntpClient);
		} catch(Throwable t){
			throw new RuntimeException(t);
		}
	}
	
	public static abstract class ClockTaskExecutor {
		
		// execute AsyncTask thread
		private void start(String host){
			new ClockTask().execute(host);
		}
		
		public abstract void onFinish(Boolean val);
	}
	
	/**
	 * Async NTP server connector
	 *
	 * Executes a NTP server connection
	 * with reflection by SDK version
	 */
	private static class ClockTask extends AsyncTask<String,Void,Boolean> {

		@Override
		protected Boolean doInBackground(String[] p1){
			try {
				String host = p1[0];
				if(SDK_INT < 24){
					Method method = CLASS.getDeclaredMethod("requestTime",String.class,int.class);
					method.setAccessible(true);
					return Boolean.valueOf(method.invoke(sntpClient,host,TIMEOUT));
				} else if(SDK_INT >= 24 && SDK_INT < 28){
					InetAddress addr = InetAddress.getByName(host);
					Method method = CLASS.getDeclaredMethod("requestTime",InetAddress.class,int.class,int.class);
					method.setAccessible(true);
					return Boolean.valueOf(method.invoke(sntpClient,addr,NTP_PORT,TIMEOUT));
				} else if(SDK_INT >= 28){
					ConnectivityManager mgr = (ConnectivityManager) mCtx.getSystemService(Context.CONNECTIVITY_SERVICE);
					Network net = mgr.getActiveNetwork();
					InetAddress addr = InetAddress.getByName(host);
					Method method = CLASS.getDeclaredMethod("requestTime",InetAddress.class,int.class,int.class,Network.class);
					method.setAccessible(true);
					return Boolean.valueOf(method.invoke(sntpClient,addr,NTP_PORT,TIMEOUT,net));
				}
				return false;
			} catch(Throwable t){
				throw new RuntimeException(t);
			}
		}

		@Override
		protected void onPostExecute(Boolean result){
			super.onPostExecute(result);
			exec.onFinish(result);
		}
	}
}
