package org.droidtr.sysapi;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils.TruncateAt;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends Activity {
	
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
    }

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		TextView tv = ActionBarUtils.getTitleView(MainActivity.this);
		if(tv != null){
			tv.setVisibility(View.VISIBLE);
			tv.setTextColor(0xFFFF0000);
			if(tv.getId() == android.R.id.title)
				((View) tv.getParent()).setBackgroundColor(0);
			tv.setText("!!! Action Bar Title Reflected !!! - This text is too long because we're testing scrolling title - We need too much text for title bar");
			tv.setEllipsize(TruncateAt.MARQUEE);
			tv.setMarqueeRepeatLimit(-1);
			tv.setSelected(true);
			tv.setTypeface(Typeface.DEFAULT_BOLD,Typeface.ITALIC);
		}
	}
}
