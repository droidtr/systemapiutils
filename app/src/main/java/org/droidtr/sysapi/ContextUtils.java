package org.droidtr.sysapi;

import android.content.Context;
import java.util.ArrayList;
import java.util.HashMap;

public class ContextUtils {
	
	private ContextUtils(){}
	
	private static HashMap<Class<?>,String> clsList;
	
	static {
		clsList = new HashMap<>();
		put("android.view.accessibility.AccessibilityManager", Context.ACCESSIBILITY_SERVICE);
		put("android.view.accessibility.CaptioningManager", Context.CAPTIONING_SERVICE);
		put("android.accounts.AccountManager", Context.ACCOUNT_SERVICE);
		put("android.app.ActivityManager", Context.ACTIVITY_SERVICE);
		put("android.app.AlarmManager", Context.ALARM_SERVICE);
		put("android.media.AudioManager", Context.AUDIO_SERVICE);
		put("android.media.MediaRouter", Context.MEDIA_ROUTER_SERVICE);
		put("android.bluetooth.BluetoothManager", Context.BLUETOOTH_SERVICE);
		put("android.view.textclassifier.TextClassificationManager", Context.TEXT_CLASSIFICATION_SERVICE);
		put("android.content.ClipboardManager", Context.CLIPBOARD_SERVICE);
		try {
			clsList.put(android.text.ClipboardManager.class, Context.CLIPBOARD_SERVICE);
			clsList.remove(android.content.ClipboardManager.class);
		} catch(Throwable t){}
		put("android.net.ConnectivityManager", Context.CONNECTIVITY_SERVICE);
		put("android.app.admin.DevicePolicyManager", Context.DEVICE_POLICY_SERVICE);
		put("android.app.DownloadManager", Context.DOWNLOAD_SERVICE);
		put("android.os.BatteryManager", Context.BATTERY_SERVICE);
		put("android.nfc.NfcManager", Context.NFC_SERVICE);
		put("android.os.DropBoxManager", Context.DROPBOX_SERVICE);
		put("android.hardware.input.InputManager", Context.INPUT_SERVICE);
		put("android.hardware.display.DisplayManager", Context.DISPLAY_SERVICE);
		put("android.view.inputmethod.InputMethodManager", Context.INPUT_METHOD_SERVICE);
		put("android.view.textservice.TextServicesManager.class", Context.TEXT_SERVICES_MANAGER_SERVICE);
		put("android.app.KeyguardManager", Context.KEYGUARD_SERVICE);
		put("android.view.LayoutInflater", Context.LAYOUT_INFLATER_SERVICE);
		put("android.location.LocationManager", Context.LOCATION_SERVICE);
		put("android.app.NotificationManager", Context.NOTIFICATION_SERVICE);
		put("android.net.nsd.NsdManager", Context.NSD_SERVICE);
		put("android.os.PowerManager", Context.POWER_SERVICE);
		put("android.os.RecoverySystem", "recovery");
		put("android.app.SearchManager", Context.SEARCH_SERVICE);
		put("android.hardware.SensorManager", Context.SENSOR_SERVICE);
		put("android.app.StatusBarManager", "statusbar");
		put("android.os.storage.StorageManager", Context.STORAGE_SERVICE);
		put("android.app.usage.StorageStatsManager", Context.STORAGE_STATS_SERVICE);
		put("android.telephony.TelephonyManager", Context.TELEPHONY_SERVICE);
		put("android.telephony.SubscriptionManager", Context.TELEPHONY_SUBSCRIPTION_SERVICE);
		put("android.telephony.CarrierConfigManager", Context.CARRIER_CONFIG_SERVICE);
		put("android.telecom.TelecomManager", Context.TELECOM_SERVICE);
		put("android.app.UiModeManager", Context.UI_MODE_SERVICE);
		put("android.hardware.usb.UsbManager", Context.USB_SERVICE);
		put("android.os.Vibrator", Context.VIBRATOR_SERVICE);
		put("android.app.WallpaperManager", Context.WALLPAPER_SERVICE);
		put("android.net.wifi.WifiManager", Context.WIFI_SERVICE);
		put("android.net.wifi.p2p.WifiP2pManager", Context.WIFI_P2P_SERVICE);
		put("android.net.wifi.aware.WifiAwareManager", Context.WIFI_AWARE_SERVICE);
		put("android.view.WindowManager", Context.WINDOW_SERVICE);
		put("android.app.AppOpsManager", Context.APP_OPS_SERVICE);
		put("android.hardware.camera2.CameraManager", Context.CAMERA_SERVICE);
		put("android.content.pm.LauncherApps", Context.LAUNCHER_APPS_SERVICE);
		put("android.content.RestrictionsManager", Context.RESTRICTIONS_SERVICE);
		put("android.print.PrintManager", Context.PRINT_SERVICE);
		put("android.companion.CompanionDeviceManager", Context.COMPANION_DEVICE_SERVICE);
		put("android.hardware.ConsumerIrManager", Context.CONSUMER_IR_SERVICE);
		put("android.media.session.MediaSessionManager", Context.MEDIA_SESSION_SERVICE);
		put("android.hardware.fingerprint.FingerprintManager", Context.FINGERPRINT_SERVICE);
		put("android.media.tv.TvInputManager", Context.TV_INPUT_SERVICE);
		put("android.app.usage.UsageStatsManager", Context.USAGE_STATS_SERVICE);
		put("android.app.usage.NetworkStatsManager", Context.NETWORK_STATS_SERVICE);
		put("android.app.job.JobScheduler", Context.JOB_SCHEDULER_SERVICE);
		put("android.media.projection.MediaProjectionManager", Context.MEDIA_PROJECTION_SERVICE);
		put("android.appwidget.AppWidgetManager", Context.APPWIDGET_SERVICE);
		put("android.media.midi.MidiManager", Context.MIDI_SERVICE);
		put("android.os.HardwarePropertiesManager", Context.HARDWARE_PROPERTIES_SERVICE);
		put("android.content.pm.ShortcutManager", Context.SHORTCUT_SERVICE);
		put("android.os.health.SystemHealthManager", Context.SYSTEM_HEALTH_SERVICE);
		put("android.view.autofill.AutofillManager", "autofill");
	}
	
	private static void put(String cls, String name){
		try {
			put(Class.forName(cls), name);
		} catch(Throwable t){}
	}
	
	private static void put(Class<?> cls, String name){
		try {
			clsList.put(cls, name);
		} catch(Throwable t){}
	}
	
	public static Class<?> getSystemServiceClass(String name){
		if(clsList.containsValue(name)){
			ArrayList<Class<?>> keys = new ArrayList<Class<?>>(clsList.keySet());
			for(Class<?> cls : keys){
				if(clsList.get(cls).equals(name)){
					return cls;
				}
			}
		}
		return null;
	}
	
	public static Object getSystemService(Context ctx, String name){
		return getSystemService(ctx, name, false);
	}
	
	public static Object getSystemService(Context ctx, String name, boolean useSystemApi){
		return useSystemApi ? ctx.getSystemService(name)
							:  getSystemService(ctx, getSystemServiceClass(name));
	}
	
	public static Object getSystemService(Context ctx, Class<?> cls){
		String serviceName = getSystemServiceName(cls);
		return serviceName != null ? ctx.getSystemService(serviceName) : null;
	}
	
	public static String getSystemServiceName(Class<?> cls){
		return clsList.containsKey(cls) ? clsList.get(cls) : null;
	}
	
}
