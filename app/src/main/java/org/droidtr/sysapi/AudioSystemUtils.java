package org.droidtr.sysapi;

import java.lang.reflect.Method;

public class AudioSystemUtils {
	
	private AudioSystemUtils(){}

	// android.media.AudioSystem.newAudioSessionId()
	public static final int newAudioSessionId(){
		try {
			Class<?> CLASS = Class.forName("android.media.AudioSystem");
			Method METHOD = CLASS.getDeclaredMethod("newAudioSessionId");
			METHOD.setAccessible(true);
			return (int) METHOD.invoke(null);
		} catch(Throwable t){}
		return -1;
	}

}
