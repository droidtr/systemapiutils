package org.droidtr.sysapi;

import android.app.Activity;
import android.os.Build;
import android.util.Log;
import android.widget.TextView;
import java.lang.reflect.Field;

public class ActionBarUtils {
	
	private ActionBarUtils(){}
	
	public static TextView getTitleView(Activity act){
		int sdk = Build.VERSION.SDK_INT;
		if(sdk >= 11 && act.getActionBar() != null){
			try {
				if(sdk < 21){
					Object temp = getField(Activity.class,act,"mActionBar");
					temp = getField(temp,"mActionView");
					return (TextView) getField(temp,"mTitleView");
				} else {
					Object temp = getField(Activity.class,act,"mActionBar");
					temp = getField(temp,"mDecorToolbar");
					if(temp.getClass().getSimpleName().equals("ActionBarView")){
						return (TextView) getField(temp,"mTitleView");
					}
					temp = getField(temp,"mToolbar");
					return (TextView) getField(temp,"mTitleTextView");
				}
			} catch(Throwable t){
				Log.e("ActionBarUtils","An error occurred",t);
			}
		}
		return act.findViewById(android.R.id.title);
	}
	
	private static Object getField(Object obj, String field) throws Throwable {
		return getField(obj.getClass(),obj,field);
	}
	
	private static Object getField(Class<?> clz, Object obj, String field) throws Throwable {
		try {
			Field f = clz.getDeclaredField(field);
			f.setAccessible(true);
			return f.get(obj);
		} catch(Throwable t){
			throw t;
		}
	}
	
}
