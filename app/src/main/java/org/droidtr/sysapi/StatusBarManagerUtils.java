package org.droidtr.sysapi;

import android.content.Context;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static android.os.Build.VERSION.SDK_INT;

public class StatusBarManagerUtils {
	
	private StatusBarManagerUtils(){}
	
	private static final boolean NEW_SDK = SDK_INT > 16;
	private static final String SERVICE_NAME = "statusbar";
	
	public static void expandNotificationsPanel(Context ctx) throws SecurityException, NoSuchMethodException, InvocationTargetException, 
													IllegalAccessException, IllegalArgumentException {
		Method showsb = getServiceClass().getMethod(NEW_SDK ? "expandNotificationsPanel" : "expand");
		showsb.invoke(getService(ctx));
    }
	
	public static void expandSettingsPanel(Context ctx) throws SecurityException, NoSuchMethodException, InvocationTargetException, 
																IllegalAccessException, IllegalArgumentException {
		Method showsb = getServiceClass().getMethod(NEW_SDK ? "expandSettingsPanel" : "expand");
		showsb.invoke(getService(ctx));
    }
	
	public static void collapsePanels(Context ctx) throws SecurityException, NoSuchMethodException, InvocationTargetException, 
													IllegalAccessException, IllegalArgumentException {
		Method hidesb = getServiceClass().getMethod(NEW_SDK ? "collapsePanels" : "collapse");
		hidesb.invoke(getService(ctx));
    }
	
	private static Object getService(Context ctx){
		return ContextUtils.getSystemService(ctx, SERVICE_NAME);
	}
	
	private static Class<?> getServiceClass(){
		return ContextUtils.getSystemServiceClass(SERVICE_NAME);
	}
	
}
