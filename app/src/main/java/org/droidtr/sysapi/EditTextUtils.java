package org.droidtr.sysapi;

import android.os.Build;
import android.widget.EditText;
import android.widget.TextView;
import java.lang.reflect.Field;

public class EditTextUtils {
	
	private EditTextUtils(){}
	
	public static void setCursor(EditText textField, int resId){
		try {
			if(Build.VERSION.SDK_INT < 28){
				Field f = TextView.class.getDeclaredField("mCursorDrawableRes");
				f.setAccessible(true);
				f.set(textField, resId);
			} else {
				Field f = TextView.class.getDeclaredField("mCursorDrawable");
				f.setAccessible(true);
				f.set(textField, textField.getResources().getDrawable(resId));
			}
		} catch(Throwable t){}
	}
	
}
